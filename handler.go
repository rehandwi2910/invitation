package main

import (
    "github.com/gin-gonic/gin"
    "log"
    "net/http"
)

type WaConnect struct {
    PhoneNumber string `json:"phone_number" binding:"required"`
}

func (s *Server) SendInvitationHandler(c *gin.Context) {
    f, err := c.FormFile("file")
    if err != nil {
        log.Println(err)
        c.AbortWithStatusJSON(http.StatusBadRequest, ErrorResponse{
            BaseResponse{
                Message: err.Error(),
                Status:  "failed",
            },
        })
        return
    }

    message := c.PostForm("message")
    if message == "" {
        c.AbortWithStatusJSON(http.StatusBadRequest, ErrorResponse{
            BaseResponse{
                Message: "masukan pesan/message",
                Status:  "failed",
            },
        })
        return
    }

    from := c.PostForm("from")
    if from == "" {
        c.AbortWithStatusJSON(http.StatusBadRequest, ErrorResponse{
            BaseResponse{
                Message: "masukan from/phone number pengirim",
                Status:  "failed",
            },
        })
        return
    }

    req := SendInvitationRequest{
        File:        f,
        Message:     message,
        PhoneNumber: from,
    }

    err = s.SendInvitation(c, &req)
    if err != nil {
        log.Println(err)
        c.AbortWithStatusJSON(http.StatusBadRequest, ErrorResponse{
            BaseResponse{
                Message: err.Error(),
                Status:  "failed",
            },
        })
        return
    }

    c.JSON(http.StatusOK, SuccessResponse{
        BaseResponse: BaseResponse{
            Message: "sukses mengirim pesan ke wa",
            Status:  "success",
        },
    })

    return
}

func (s *Server) SendMessageHandler(c *gin.Context) {
    req := new(SendMessageRequest)
    if err := c.ShouldBindJSON(req); err != nil {
        log.Println(err)
        c.AbortWithStatusJSON(http.StatusBadRequest, ErrorResponse{
            BaseResponse{
                Message: err.Error(),
                Status:  "failed",
            },
        })
        return
    }

    err := s.SendMessage(c, req)
    if err != nil {
        log.Println(err)
        c.AbortWithStatusJSON(http.StatusBadRequest, ErrorResponse{
            BaseResponse{
                Message: err.Error(),
                Status:  "failed",
            },
        })
        return
    }

    c.JSON(http.StatusOK, SuccessResponse{
        BaseResponse: BaseResponse{
            Message: "sukses mengirim pesan wa ke " + req.To,
            Status:  "success",
        },
    })

    return
}

func (s *Server) CheckWaHandler(c *gin.Context) {
    phone := c.Param("phone")
    if phone == "" {
        c.AbortWithStatusJSON(http.StatusBadRequest, ErrorResponse{
            BaseResponse{
                Message: "tolong masukan nomor telepon",
                Status:  "failed",
            },
        })
        return
    }

    client, err := s.GetWA(phone)
    if err != nil {
        log.Println(err)
        c.AbortWithStatusJSON(http.StatusBadRequest, ErrorResponse{
            BaseResponse{
                Message: err.Error(),
                Status:  "failed",
            },
        })
        return
    }

    if client == nil {
        log.Println(err)
        c.AbortWithStatusJSON(http.StatusBadRequest, ErrorResponse{
            BaseResponse{
                Message: "nomor whatsapp belum terdaftar",
                Status:  "failed",
            },
        })
        return
    }

    c.JSON(http.StatusOK, SuccessResponse{
        BaseResponse: BaseResponse{
            Message: "nomor wa terdaftar",
            Status:  "success",
        },
    })

    return
}

func (s *Server) ConnectWAHandler(c *gin.Context) {
    req := new(WaConnect)
    if err := c.ShouldBindJSON(req); err != nil {
        log.Println(err)
        c.AbortWithStatusJSON(http.StatusBadRequest, ErrorResponse{
            BaseResponse{
                Message: err.Error(),
                Status:  "failed",
            },
        })
        return
    }

    qr, err := s.ConnectWA(req.PhoneNumber)
    if err != nil {
        log.Println(err)
        c.AbortWithStatusJSON(http.StatusBadRequest, ErrorResponse{
            BaseResponse{
                Message: err.Error(),
                Status:  "failed",
            },
        })
        return
    }

    c.JSON(http.StatusOK, SuccessResponse{
        BaseResponse: BaseResponse{
            Message: "harap scan qr ini ke wa",
            Status:  "success",
        },
        Data: qr,
    })

    return
}
