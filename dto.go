package main

import (
    "errors"
    "mime/multipart"
)

var ErrWaNotFound = errors.New("nomor whatsapp belum terdaftar")

type SendMessageRequest struct {
    From    string `json:"from" binding:"required"`
    To      string `json:"to" binding:"required"`
    Message string `json:"message" binding:"required"`
}

type SendInvitationRequest struct {
    File        *multipart.FileHeader `json:"file"`
    Message     string                `json:"message"`
    PhoneNumber string                `json:"phone_number"`
}

var tmpMessage = `Kepada Yth.
Bapak/Ibu/Saudara/i
*{NAMA}*

Tanpa mengurangi rasa hormat, perkenankan kami mengundang Bapak/Ibu/Saudara/i,
untuk menghadiri acara pernikahan kami:

Berikut link untuk info lengkap dari acara kami:

{LINK_UNDANGAN}

Kami mengharap kehadiran Bapak/Ibu/Saudara/i sekalian untuk memberikan doa restu.

Terima kasih
`
