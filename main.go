package main

import (
    "context"
    "github.com/gin-gonic/gin"
    "github.com/jmoiron/sqlx"
    _ "github.com/mattn/go-sqlite3"
    "go.mau.fi/whatsmeow"
    "go.mau.fi/whatsmeow/store/sqlstore"
    waLog "go.mau.fi/whatsmeow/util/log"
    "go.uber.org/zap"
    "log"
    "net/http"
    "os"
    "os/signal"
    "syscall"
    "time"
)

type Server struct {
    container    *sqlstore.Container
    r            *gin.Engine
    registeredWa RegisteredWAMap
    db           *sqlx.DB
    log          *zap.Logger
}

func main() {
    log.SetFlags(log.Lshortfile | log.Lmicroseconds)
    ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
    defer func() {
        cancel()
    }()

    s := Server{}

    logger, _ := zap.NewProduction()
    defer logger.Sync()
    s.log = logger

    s.r = gin.New()
    s.r.Use(gin.Logger(), gin.Recovery())

    s.registeredWa = make(map[string]*whatsmeow.Client)
    err := s.connectWADB()
    if err != nil {
        log.Fatalf("can't start database wa")
    }

    err = s.connectDB()
    if err != nil {
        log.Fatalf("can't start database")
    }

    err = s.migrateTable()
    if err != nil {
        log.Fatalf("can't migrate table")
    }

    devices, err := s.container.GetAllDevices()
    for _, d := range devices {
        client := whatsmeow.NewClient(d, waLog.Noop)
        if client == nil {
            log.Fatalf("client nil %+v", client)
        }
        s.registeredWa.Add(d.ID.User, client)
    }

    s.r.POST("/wa/register", s.ConnectWAHandler)
    s.r.GET("/wa/:phone", s.CheckWaHandler)
    s.r.POST("/wa/send", s.SendMessageHandler)
    s.r.POST("/wa/invitation", s.SendInvitationHandler)

    srv := &http.Server{
        Addr:    ":6969",
        Handler: s.r.Handler(),
    }

    log.Printf("Server started on port %s", srv.Addr)

    go func() {
        if err := srv.ListenAndServe(); err != nil {
            log.Println("can't start app on port " + srv.Addr)
        }
    }()

    c := make(chan os.Signal)
    signal.Notify(c, os.Interrupt, syscall.SIGTERM)
    <-c

    if err := s.db.Close(); err != nil {
        log.Println(err)
    }
    srv.Shutdown(ctx)
}

func (s *Server) connectWADB() (err error) {
    dbLog := waLog.Stdout("Database", "DEBUG", true)
    container, err := sqlstore.New("sqlite3", "file:../wa.db?_foreign_keys=on", dbLog)
    if err != nil {
        log.Println(err)
        return err
    }

    s.container = container

    return nil
}

func (s *Server) connectDB() (err error) {
    s.db, err = sqlx.Connect("sqlite3", "../invitation.db")
    if err != nil {
        log.Println(err)
        return err
    }

    return nil
}

func (s *Server) migrateTable() (err error) {
    query := "CREATE TABLE IF NOT EXISTS employees (id uuid NOT NULL, full_name varchar(255) NOT NULL, from_phone varchar(30), to_phone varchar(30) NOT NULL, status bool DEFAULT false, err_message text);"
    _, err = s.db.Exec(query)
    if err != nil {
        log.Println(err)
        return err
    }

    return nil
}
