package main

import (
    "context"
    "database/sql"
    "encoding/base64"
    "errors"
    "fmt"
    "github.com/google/uuid"
    _ "github.com/mattn/go-sqlite3"
    "github.com/xuri/excelize/v2"
    "go.mau.fi/whatsmeow"
    waProto "go.mau.fi/whatsmeow/binary/proto"
    "go.mau.fi/whatsmeow/store"
    "go.mau.fi/whatsmeow/types"
    "go.mau.fi/whatsmeow/types/events"
    waLog "go.mau.fi/whatsmeow/util/log"
    "go.uber.org/zap"
    "google.golang.org/protobuf/proto"
    "log"
    "net/url"
    "rsc.io/qr"
    "strings"
    "time"
)

const (
    LINK_INVITATION = "https://ngantenstory.id/eti-rehan"
)

type Person struct {
    Name    string `json:"name"`
    TypeInv string `json:"type_inv"`
    Phone   string `json:"phone"`
}

func (p *Person) IsDigital() (ok bool) {
    if p.TypeInv == "DIGITAL" {
        return true
    }

    return false
}

func (p *Person) IsPhoneExist() (ok bool) {
    if p.Phone != "" {
        phoneCode := p.Phone[:2]
        if phoneCode != "62" {
            return false
        }

        // p.CleanPhoneNumber()
        return true
    }

    return false
}

func (p *Person) CleanPhoneNumber() {
    space := strings.ReplaceAll(p.Phone, " ", "")
    dash := strings.ReplaceAll(space, "-", "")
    p.Phone = dash
}

type PersonMap map[string]Person

func eventHandler(evt interface{}) {
    switch v := evt.(type) {
    case *events.Message:
        fmt.Println("Received a message!", v.Message.GetConversation())
    }
}

type RegisteredWAMap map[string]*whatsmeow.Client

func (rm RegisteredWAMap) Add(phoneNumber string, client *whatsmeow.Client) {
    _, ok := rm[phoneNumber]
    if !ok {
        rm[phoneNumber] = client
    }

}

func (rm RegisteredWAMap) Get(phoneNumber string) (client *whatsmeow.Client) {
    el, ok := rm[phoneNumber]
    if !ok {
        return nil
    }

    return el
}

func (s *Server) SendInvitation(ctx context.Context, req *SendInvitationRequest) (err error) {
    successSendMap := make(map[string]Person)
    errSendMap := make(map[string]Person)
    empMap, err := s.loadData(ctx, req)
    if err != nil {
        log.Println(err)
        return err
    }

    c, err := s.GetWA(req.PhoneNumber)
    if err != nil {
        log.Println(err)
        return err
    }

    err = c.Connect()
    if err != nil {
        log.Println(err)
        return err
    }
    defer c.Disconnect()

    log.Printf("sheetMap %+v", empMap)
    ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
    defer cancel()

    for _, emp := range empMap {
        p := Person{
            Name:    emp.Name,
            TypeInv: emp.TypeInv,
            Phone:   emp.Phone,
        }

        data := Employee{
            FullName:  emp.Name,
            ToPhone:   emp.Phone,
            FromPhone: req.PhoneNumber,
            Status:    true,
        }

        empResult := Employee{}
        query := "SELECT * FROM employees WHERE to_phone = $1 AND status = $2"
        err := s.db.Get(&empResult, query, emp.Phone, true)
        if err != sql.ErrNoRows && err != nil {
            s.log.Error("s.db.Get(&empResult, query, emp.Phone, true)", zap.String("error", err.Error()))
            err = s.insertData(&data, err)
            if err != nil {
                log.Println(err)
            }
            continue
        }

        if empResult.ID != uuid.Nil {
            s.log.Info("empResult.ID != uuid.Nil ", zap.String("error", "pesan sudah dikirim ke nomor ini "+data.ToPhone))
            continue
        }

        linkInvit := fmt.Sprintf("%s?to=%s", LINK_INVITATION, url.QueryEscape(emp.Name))
        replaceLink := strings.Replace(req.Message, "{LINK_UNDANGAN}", linkInvit, -1)
        replaceName := strings.Replace(replaceLink, "{NAMA}", emp.Name, -1)
        log.Printf("proses sending message: %s", emp.Phone)
        jid := types.JID{
            User:   emp.Phone,
            Server: types.DefaultUserServer,
        }
        msg := waProto.Message{Conversation: proto.String(replaceName)}
        _, err = c.SendMessage(ctx, jid, &msg)
        if err != nil {
            s.log.Error("error send message to "+emp.Phone, zap.String("error", err.Error()))
            errSendMap[emp.Phone] = p

            err = s.insertData(&data, err)
            if err != nil {
                s.log.Error("s.insertData", zap.String("error", err.Error()))
            }
            continue
        }

        err = s.insertData(&data, err)
        if err != nil {
            s.log.Error("s.insertData", zap.String("error", err.Error()))
        }

        log.Println("message sent")
        successSendMap[emp.Phone] = p

    }

    return nil
}

func (s *Server) insertData(data *Employee, e error) (err error) {
    if e != nil {
        data.ErrMessage = sql.NullString{e.Error(), true}
        data.Status = false
    }
    data.GenerateUUID()
    query := "INSERT INTO employees (id, full_name, from_phone, to_phone, status, err_message) " +
        "VALUES (:id, :full_name, :from_phone, :to_phone, :status, :err_message)"
    _, err = s.db.NamedExec(query, data)
    if err != nil {
        log.Println(err)
        return err
    }

    return nil
}

func (s *Server) loadData(ctx context.Context, req *SendInvitationRequest) (res map[string]Person, err error) {
    fOpen, err := req.File.Open()
    if err != nil {
        log.Println(err)
        return res, err
    }
    f, err := excelize.OpenReader(fOpen)
    if err != nil {
        log.Println(err)
        return res, err
    }

    defer func() {
        if err := f.Close(); err != nil {
            fmt.Println(err)
        }
    }()

    sheets := f.GetSheetList()
    empMap := make(map[string]Person)

    for _, s := range sheets {
        rows, err := f.GetRows(s)
        if err != nil {
            log.Println(err)
            return res, err
        }

        for i, _ := range rows {
            i++
            name, err := f.GetCellValue(s, fmt.Sprintf("B%d", i))
            if err != nil {
                log.Println(err)
                return res, err
            }

            typeInv, err := f.GetCellValue(s, fmt.Sprintf("D%d", i))
            if err != nil {
                log.Println(err)
                return res, err
            }

            phone, err := f.GetCellValue(s, fmt.Sprintf("E%d", i))
            if err != nil {
                log.Println(err)
                return res, err
            }

            p := Person{
                Name:    name,
                TypeInv: typeInv,
                Phone:   phone,
            }

            if !p.IsDigital() {
                continue
            }

            if !p.IsPhoneExist() {
                continue
            }
            empMap[p.Phone] = p
        }
    }

    return empMap, nil
}

func (s *Server) SendMessage(ctx context.Context, req *SendMessageRequest) (err error) {
    ctxCancel, cancel := context.WithTimeout(ctx, 5*time.Second)
    defer cancel()

    c := s.registeredWa.Get(req.From)
    if c == nil {
        return errors.New("nomor wa belum terdaftar")
    }

    err = c.Connect()
    defer c.Disconnect()
    if err != nil {
        log.Println(err)
        return err
    }

    to := types.JID{
        User:   req.To,
        Server: types.DefaultUserServer,
    }

    msg := waProto.Message{
        Conversation: proto.String(req.Message),
    }

    _, err = c.SendMessage(ctxCancel, to, &msg)
    if err != nil {
        log.Println(err)
        return err
    }

    return nil
}

func (s *Server) ConnectWA(phoneNumber string) (res string, err error) {
    client, err := s.GetWA(phoneNumber)
    if err != ErrWaNotFound && err != nil {
        log.Println(err)
        return res, err
    }

    if client != nil {
        log.Println("nomor whatsapp telah terdaftar")
        return res, errors.New("nomor whatsapp telah terdaftar")
    }

    jid := types.JID{
        User:   phoneNumber,
        Server: types.DefaultUserServer,
    }
    var storeDevice *store.Device
    storeDevice, err = s.container.GetDevice(jid)
    if err != nil {
        log.Println(err)
        return res, err
    }

    if storeDevice == nil {
        storeDevice = s.container.NewDevice()
    }

    c := whatsmeow.NewClient(storeDevice, waLog.Noop)

    qrChan, err := c.GetQRChannel(context.Background())
    if err != nil {
        log.Println(err)
        return res, err
    }
    err = c.Connect()
    if err != nil {
        log.Println(err)
        return res, err
    }
    for evt := range qrChan {
        if evt.Event == "code" {
            fmt.Println("QR code:", evt.Code)
            code, err := qr.Encode(evt.Code, qr.L)
            if err != nil {
                log.Println(err)
                return res, err
            }

            res = base64.StdEncoding.EncodeToString(code.PNG())
            break
        } else {
            fmt.Println("Login event:", evt.Event)
        }
    }

    s.registeredWa.Add(phoneNumber, client)
    return res, nil
}

func (s *Server) GetWA(phoneNumber string) (waClient *whatsmeow.Client, err error) {
    client := s.registeredWa.Get(phoneNumber)
    if client == nil {
        return nil, ErrWaNotFound
    }

    return client, nil
}
