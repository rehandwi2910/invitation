package main

import (
    "database/sql"
    "github.com/google/uuid"
)

type Employee struct {
    ID         uuid.UUID      `json:"id" db:"id"`
    FullName   string         `json:"full_name" db:"full_name"`
    FromPhone  string         `json:"from_phone" db:"from_phone"`
    ToPhone    string         `json:"to_phone" db:"to_phone"`
    Status     bool           `json:"status" db:"status"`
    ErrMessage sql.NullString `json:"err_message" db:"err_message"`
}

func (e *Employee) GenerateUUID() {
    e.ID = uuid.New()
}
