package main

type ErrorResponse struct {
    BaseResponse
}

type SuccessResponse struct {
    BaseResponse
    Data interface{} `json:"data,omitempty"`
}

type BaseResponse struct {
    Message string `json:"message"`
    Status  string `json:"status"`
}
